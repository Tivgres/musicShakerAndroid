package co.tivgres.musicshaker.controllers.qr.validator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Shaker
 * Created by tivgres on 11:28 PM | 17/02/2018.
 */
public class FedValidatorTest {

    private String base64 = "eyJhZGRyZXNzIjoiMTkyLjE2OC44OC4zIiwibG9naW4iOiJzb21lVGV4dExvZ2luIiwicGFzc3dvcmQiOiJzb21lVGV4dFBhc3N3b3JkIiwicG9ydCI6MjEyMX0=";
    private String base64Empty = "eyJwb3J0IjowfQ==";
    private String base64Wrong = "ew0KICAidHlwZSI6Im9iamVjdCIsDQogICJwcm9wZXJ0aWVzIjogew0KICAgICJmb28iOiB7DQogICAgICAidHlwZSI6ICJzdHJpbmciDQogICAgfSwNCiAgICAiYmFyIjogew0KICAgICAgInR5cGUiOiAiaW50ZWdlciINCiAgICB9LA0KICAgICJiYXoiOiB7DQogICAgICAidHlwZSI6ICJib29sZWFuIg0KICAgIH0NCiAgfQ0KfQ==";
    private String clearString = "FedValidatorTest";
    private FedValidator fedValidator;

    @Before
    public void setUp() throws Exception {
        fedValidator = new FedValidator();
    }

    @Test
    public void isFed() throws Exception {
        assertEquals(true, fedValidator.isFed(base64));
        assertEquals(false, fedValidator.isFed(base64Empty));
        assertEquals(false, fedValidator.isFed(base64Wrong));
        assertEquals(false, fedValidator.isFed(clearString));
    }

    @Test
    public void getFed() throws Exception {
        fedValidator.isFed(base64);
        assertEquals(true, fedValidator.getFed() != null);
        assertEquals(true, fedValidator.getFed().getAddress() != null);
        assertEquals(true, fedValidator.getFed().getPort() != 0);
        assertEquals(true, fedValidator.getFed().getLogin() != null);
        assertEquals(true, fedValidator.getFed().getPassword() != null);
    }


}