package co.tivgres.musicshaker.ui.main.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.tivgres.musicshaker.R;
import co.tivgres.musicshaker.models.FtpEntryData;
import co.tivgres.musicshaker.ui.base.BaseFragment;
import co.tivgres.musicshaker.ui.main.MainActivity;
import co.tivgres.musicshaker.ui.main.iCallbackFed;

/**
 * Shaker
 * Created by tivgres on 12:57 AM | 13/02/2018.
 */

public class FedFragment extends BaseFragment {

    @BindView(R.id.fed_fragment_btn_connect)
    AppCompatButton btnConnect;

    @BindView(R.id.fed_fragment_dialog_et_ip)
    MaterialEditText etIp;

    @BindView(R.id.fed_fragment_dialog_et_port)
    MaterialEditText etPort;

    @BindView(R.id.fed_fragment_dialog_et_login)
    MaterialEditText etLogin;

    @BindView(R.id.fed_fragment_dialog_et_password)
    MaterialEditText etPass;
    private iCallbackFed iCallbackFed;
    private ShimmerFrameLayout container;
    private Unbinder unbinder;
    private InputFilter[] filters = new InputFilter[1];

    @NonNull
    public static FedFragment newInstance() {
        return new FedFragment();
    }

    @OnClick(R.id.fed_fragment_btn_connect)
    void connect() {
        if (checkEntries()) {
            try {
                FtpEntryData fed = new FtpEntryData();
                fed.setAddress(etIp.getText().toString());
                fed.setPort(Integer.parseInt(etPort.getText().toString()));
                fed.setLogin(etLogin.getText().toString());
                fed.setPassword(etPass.getText().toString());
                iCallbackFed.goToNext(fed);
            } catch (NumberFormatException ignored) {
                Toast.makeText(this.getContext(), "Wrong port", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fed, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        iCallbackFed = (MainActivity) getActivity();
        setupFilter();
        etIp.setFilters(filters);
        container = view.findViewById(R.id.shimmer_view_container);
        setupAnimation();
    }

    private boolean checkEntries() {
        return !etIp.getText().toString().isEmpty() &&
                !etPort.getText().toString().isEmpty() &&
                !etLogin.getText().toString().isEmpty() &&
                !etPass.getText().toString().isEmpty();
    }

    private void setupFilter() {
        filters[0] = (source, start, end, dest, dstart, dend) -> {
            if (end > start) {
                String destTxt = dest.toString();
                String resultingTxt = destTxt.substring(0, dstart)
                        + source.subSequence(start, end)
                        + destTxt.substring(dend);
                if (!resultingTxt
                        .matches("^\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3})?)?)?)?)?)?")) {
                    return "";
                } else {
                    String[] splits = resultingTxt.split("\\.");
                    for (String split : splits) {
                        if (Integer.valueOf(split) > 255) {
                            return "";
                        }
                    }
                }
            }
            return null;
        };
    }

    private void setupAnimation() {
        container.setAngle(ShimmerFrameLayout.MaskAngle.CW_270);
        container.setTilt(5);
        container.setDuration(3000);
        //container.startShimmerAnimation();
        container.setAutoStart(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        //container.clearAnimation();
    }

    @Override
    public void show() {
        Log.d("SHAKER_TAG", "FED SHOW");
    }

    @Override
    public void hide() {
        Log.d("SHAKER_TAG", "FED HIDE");
    }

    @Override
    public String getTitle() {
        return this.getString(R.string.app_name).concat(" ").concat(this.getString(R.string.fed_fragment_title));
    }
}
