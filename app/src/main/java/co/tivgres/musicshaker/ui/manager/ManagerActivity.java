package co.tivgres.musicshaker.ui.manager;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import co.tivgres.musicshaker.models.FtpEntryData;
import co.tivgres.musicshaker.ui.main.MainActivity;

import static co.tivgres.musicshaker.ui.main.MainActivity.EXTRAS_ERROR_TAG;
import static co.tivgres.musicshaker.ui.main.MainActivity.EXTRAS_IP_TAG;
import static co.tivgres.musicshaker.ui.main.MainActivity.EXTRAS_LOGIN_TAG;
import static co.tivgres.musicshaker.ui.main.MainActivity.EXTRAS_PASSWORD_TAG;
import static co.tivgres.musicshaker.ui.main.MainActivity.EXTRAS_PORT_TAG;

/**
 * Shaker
 * Created by tivgres on 11:22 PM | 12/02/2018.
 */

public class ManagerActivity extends AppCompatActivity {

    private FtpEntryData fed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            getExtras(extras);
        }
        if (!checkFed()) {
            wrongFed();
        }
    }

    private void getExtras(Bundle extras) {
        if (fed == null) {
            fed = new FtpEntryData();
        }
        fed.setAddress(extras.getString(EXTRAS_IP_TAG));
        fed.setPort(extras.getInt(EXTRAS_PORT_TAG));
        fed.setLogin(extras.getString(EXTRAS_LOGIN_TAG));
        fed.setPassword(extras.getString(EXTRAS_PASSWORD_TAG));
    }

    private boolean checkFed() {
        return true;
    }

    private void wrongFed() {
        Intent intent = new Intent(ManagerActivity.this, MainActivity.class);
        intent.putExtra(EXTRAS_ERROR_TAG, true);
        startActivity(intent);
        this.finish();
    }
}
