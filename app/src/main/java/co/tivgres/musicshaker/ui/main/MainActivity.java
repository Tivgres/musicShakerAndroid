package co.tivgres.musicshaker.ui.main;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import co.tivgres.musicshaker.R;
import co.tivgres.musicshaker.models.FtpEntryData;
import co.tivgres.musicshaker.ui.base.BaseFragment;
import co.tivgres.musicshaker.ui.main.adapter.MainFragmentsAdapter;
import co.tivgres.musicshaker.ui.main.fragments.FedFragment;
import co.tivgres.musicshaker.ui.main.fragments.ScannerFrag;
import co.tivgres.musicshaker.ui.manager.ManagerActivity;
import me.kaelaela.verticalviewpager.VerticalViewPager;
import me.kaelaela.verticalviewpager.transforms.StackTransformer;

public class MainActivity extends AppCompatActivity implements iCallbackFed {

    public static final String EXTRAS_IP_TAG = "EXTRAS_IP_TAG",
            EXTRAS_PORT_TAG = "EXTRAS_PORT_TAG",
            EXTRAS_LOGIN_TAG = "EXTRAS_LOGIN_TAG",
            EXTRAS_PASSWORD_TAG = "EXTRAS_PASSWORD_TAG";
    public static final String EXTRAS_ERROR_TAG = "EXTRAS_ERROR_TAG";
    private BaseFragment fedFragment, scannerFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.getBoolean(EXTRAS_ERROR_TAG)) {
                showErrorDialog();
            }
        }
        setContentView(R.layout.activity_main);
        //setTitle("");TODO make title for fragments
        initViews();
        checkPermissions();
    }

    private void showErrorDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.main_activity_error_dialog_title);
        builder.setMessage(R.string.main_activity_error_dialog_body);
        builder.setCancelable(false);
        builder.setPositiveButton("ok", (dialog, which) -> dialog.dismiss());
        builder.show();
    }

    private void initViews() {
        fedFragment = FedFragment.newInstance();
        scannerFragment = ScannerFrag.newInstance();
        VerticalViewPager viewPager = findViewById(R.id.main_activity_view_pager);
        viewPager.setPageTransformer(false, new StackTransformer());
        viewPager.setAdapter(new MainFragmentsAdapter.Holder(getSupportFragmentManager())
                .add(fedFragment)
                .add(scannerFragment)
                .set());
        viewPager.setOverScrollMode(View.OVER_SCROLL_NEVER);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        fedFragment.show();
                        scannerFragment.hide();
                        break;
                    case 1:
                        fedFragment.hide();
                        scannerFragment.show();
                        break;

                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    private void checkPermissions() {
        if (checkPermissionCamera()) {
            requestPermissionCamera();
        }
    }

    private boolean checkPermissionCamera() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED;
    }

    private void requestPermissionCamera() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 100);
    }


    @Override
    public void goToNext(@NonNull FtpEntryData fed) {
        Intent managerContent = new Intent(MainActivity.this, ManagerActivity.class);
        managerContent.putExtra(EXTRAS_IP_TAG, fed.getAddress());
        managerContent.putExtra(EXTRAS_PORT_TAG, fed.getPort());
        managerContent.putExtra(EXTRAS_LOGIN_TAG, fed.getLogin());
        managerContent.putExtra(EXTRAS_PASSWORD_TAG, fed.getPassword());
        startActivity(managerContent);
        this.finish();
    }
}
