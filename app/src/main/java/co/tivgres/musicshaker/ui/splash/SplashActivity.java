package co.tivgres.musicshaker.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import co.tivgres.musicshaker.ui.main.MainActivity;


/**
 * Shaker
 * Created by tivgres on 1:54 AM | 12/02/2018.
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startActivity(new Intent(this, MainActivity.class));
        this.finish();
    }

}
