package co.tivgres.musicshaker.ui.base;

import android.support.v4.app.Fragment;

/**
 * Shaker
 * Created by tivgres on 3:49 AM | 13/02/2018.
 */

public abstract class BaseFragment extends Fragment {

    public abstract void show();

    public abstract void hide();

    public abstract String getTitle();

}
