package co.tivgres.musicshaker.ui.main;

import co.tivgres.musicshaker.models.FtpEntryData;

/**
 * Shaker
 * Created by tivgres on 10:26 PM | 17/02/2018.
 */

public interface iCallbackFed {

    void goToNext(FtpEntryData fed);
}
