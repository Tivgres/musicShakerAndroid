package co.tivgres.musicshaker.ui.main.adapter;

import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import org.jetbrains.annotations.Contract;

import java.util.ArrayList;
import java.util.List;

import co.tivgres.musicshaker.ui.base.BaseFragment;

/**
 * Shaker
 * Created by tivgres on 4:20 AM | 13/02/2018.
 */

public class MainFragmentsAdapter extends PagerAdapter {

    private final FragmentManager fragmentManager;
    private List<BaseFragment> fragments = new ArrayList<>();
    private FragmentTransaction fragmentTransaction = null;
    private Fragment currentPrimaryItem = null;

    MainFragmentsAdapter(FragmentManager fm, List<BaseFragment> fragments) {
        this.fragmentManager = fm;
        this.fragments = fragments;
    }

    @NonNull
    @Contract(pure = true)
    private static String makeFragmentName(int viewId, long id) {
        return "android:switcher:" + viewId + ":" + id;
    }

    private BaseFragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public void startUpdate(@NonNull ViewGroup container) {
        if (container.getId() == View.NO_ID) {
            throw new IllegalStateException("ViewPager with adapter " + this
                    + " requires a view id");
        }
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        if (fragmentTransaction == null) {
            fragmentTransaction = fragmentManager.beginTransaction();
        }

        final long itemId = getItemId(position);

        // Do we already have this fragment?
        String name = makeFragmentName(container.getId(), itemId);
        Fragment fragment = fragmentManager.findFragmentByTag(name);
        if (fragment != null) {

            fragmentTransaction.attach(fragment);
        } else {
            fragment = getItem(position);

            fragmentTransaction.add(container.getId(), fragment,
                    makeFragmentName(container.getId(), itemId));
        }
        if (fragment != currentPrimaryItem) {
            fragment.setMenuVisibility(false);
            fragment.setUserVisibleHint(false);
        }
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return (fragments.get(position)).getTitle();
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        if (fragmentTransaction == null) {
            fragmentTransaction = fragmentManager.beginTransaction();
        }
        fragmentTransaction.detach((Fragment) object);
    }

    @Override
    public void setPrimaryItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        Fragment fragment = (Fragment) object;
        if (fragment != currentPrimaryItem) {
            if (currentPrimaryItem != null) {
                currentPrimaryItem.setMenuVisibility(false);
                currentPrimaryItem.setUserVisibleHint(false);
            }
            fragment.setMenuVisibility(true);
            fragment.setUserVisibleHint(true);
            currentPrimaryItem = fragment;
        }
    }

    @Override
    public void finishUpdate(@NonNull ViewGroup container) {
        if (fragmentTransaction != null) {
            fragmentTransaction.commitNowAllowingStateLoss();
            fragmentTransaction = null;
        }
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return ((Fragment) object).getView() == view;
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    /**
     * Return a unique identifier for the item at the given position.
     * <p>
     * <p>The default implementation returns the given position.
     * Subclasses should override this method if the positions of items can change.</p>
     *
     * @param position Position within this adapter
     * @return Unique identifier for the item at position
     */
    @Contract(pure = true)
    private long getItemId(int position) {
        return position;
    }

    public static class Holder {
        private final List<BaseFragment> fragments = new ArrayList<>();
        private FragmentManager manager;

        public Holder(FragmentManager manager) {
            this.manager = manager;
        }

        public MainFragmentsAdapter.Holder add(BaseFragment f) {
            fragments.add(f);
            return this;
        }

        public MainFragmentsAdapter set() {
            return new MainFragmentsAdapter(manager, fragments);
        }
    }


}
