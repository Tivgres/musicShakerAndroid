package co.tivgres.musicshaker.controllers.qr.core;

import android.hardware.Camera;
import android.support.annotation.NonNull;

import org.jetbrains.annotations.Contract;

public class CameraWrapper {
    public final Camera mCamera;
    final int mCameraId;

    private CameraWrapper(@NonNull Camera camera, int cameraId) {
        this.mCamera = camera;
        this.mCameraId = cameraId;
    }

    @Contract("null, _ -> null; !null, _ -> !null")
    static CameraWrapper getWrapper(Camera camera, int cameraId) {
        if (camera == null) {
            return null;
        } else {
            return new CameraWrapper(camera, cameraId);
        }
    }
}
