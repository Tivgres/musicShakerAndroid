package co.tivgres.musicshaker.controllers.qr.core;


import android.hardware.Camera;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;

import co.tivgres.musicshaker.ui.main.fragments.ScannerFrag;

// This code is mostly based on the top answer here: http://stackoverflow.com/questions/18149964/best-use-of-handlerthread-over-other-similar-classes
public class CameraHandlerThread extends HandlerThread {
    private static final String LOG_TAG = "CameraHandlerThread";

    //private BarcodeScannerView mScannerView;
    private ScannerFrag mScannerView;

    public CameraHandlerThread(ScannerFrag scannerView) {
        super("CameraHandlerThread");
        mScannerView = scannerView;
        start();
    }

    public void startCamera(final int cameraId) {
        Handler localHandler = new Handler(getLooper());
        localHandler.post(() -> {
            final Camera camera = CameraUtils.getCameraInstance(cameraId);
            Handler mainHandler = new Handler(Looper.getMainLooper());
            mainHandler.post(() -> mScannerView.setupCameraPreview(CameraWrapper.getWrapper(camera, cameraId)));
        });
    }
}
