package co.tivgres.musicshaker.controllers.qr.core;

import com.google.zxing.Result;

/**
 * Shaker
 * Created by tivgres on 7:13 PM | 14/02/2018.
 */

public interface ResultHandler {

    void handleResult(Result rawResult);

}
