package co.tivgres.musicshaker.controllers.qr.validator;


import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.apache.commons.net.util.Base64;

import co.tivgres.musicshaker.models.FtpEntryData;

/**
 * Shaker
 * Created by tivgres on 9:11 PM | 12/02/2018.
 */


public class FedValidator {

    private Gson gson;
    private FtpEntryData fed;

    public FedValidator() {
        gson = new Gson();
    }

    public boolean isFed(String temp) {
        try {
            fed = gson.fromJson(new String(Base64.decodeBase64(temp)), FtpEntryData.class);
            return fed.getAddress() != null &&
                    fed.getPort() != 0 &&
                    fed.getLogin() != null &&
                    fed.getPassword() != null;
        } catch (IllegalArgumentException | JsonSyntaxException ignored) {
            return false;
        }
    }

    public FtpEntryData getFed() {
        return fed;
    }


}
