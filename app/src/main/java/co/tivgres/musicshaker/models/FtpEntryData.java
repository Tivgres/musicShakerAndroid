package co.tivgres.musicshaker.models;

/**
 * Shaker
 * Created by tivgres on 8:38 PM | 12/02/2018.
 */

public class FtpEntryData {

    private String login;
    private String password;
    private String address;
    private int port;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }


}
